= Contributing
:toc: auto

== Modifying the code

. If you want to add features or change behavior, create an issue so we can discuss it
. Fork the repo
. Create a merge request to share your changes

TIP: More info about Forking workflow with Gitlab https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html[here]

TIP: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-a-fork[How to create a merge request from a fork]?

CAUTION: We are using https://danielkummer.github.io/git-flow-cheatsheet[Gitflow] as Git workflow. That means the `develop` branch is the one that contains the latest features considered implemented. The `main` branch only contains code of the different version released. You must start your work from the `develop` branch.

== Development requirements

* LabVIEW 2014 SP1 32 bit

== Dependencies

=== Mandatory

* https://www.vipm.io/package/wovalab_lib_antidoc/[Antidoc] v2.0
* https://www.vipm.io/package/wovalab_lib_asciidoc_for_labview/[Asciidoc for LabVIEW] v1.5

=== Optional

N/A

== Development guidelines

=== Conventions 

==== LabVIEW configuration

* Disable automatic error handling
* Separate compiled code from file

==== File naming

* Full plain English words using Spaced Pascal Case. (e.g., `Generate Project Documentation.vi`)
* Names must be meaningful.
* For typedefs use the following convention.

----
Xyz--enum.ctl
Xyz--cluster.ctl
Xyz--map.ctl
…
----

==== Constant VI

When a constant is needed, wrapped it in a VI.
Named it`Xyz--constant.vi`.

==== Error VI

If a custom error needs to be created, wrapped an error ring into a VI.
Named it`Xyz--error.vi`.

=== Style

==== Diagram organization

* Use https://forums.ni.com/t5/Quick-Drop-Enthusiasts/Quick-Drop-Keyboard-Shortcut-Nattify-VI/m-p/3990402[Nattify VI] quick drop action to ensure a standardize way to organize panels and diagrams.